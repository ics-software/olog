/*
 * Functions specific to login.html
 *
 * @author: Sunil Sah <sunil.sah@cosylab.com>
 */
$(document).ready(function(){
	// Show sign in form
	var template = getTemplate('template_logged_out');
	var html = Mustache.to_html(template, {"user": "User"});
	$('#top_container').html(html);
	login();
	$( "#user_login_dropdown" ).trigger( "click" );
});
