/*
 * Copyright (c) 2011 Michigan State University - Facility for Rare Isotope Beams
 */
package edu.msu.nscl.olog;

import java.security.Principal;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * Top level Jersey HTTP methods for the .../server URL
 * This handles credentials and login functionality.
 *
 * @author <a href="mailto:sunil.sah@cosylab.com">Sunil Sah</a>
 */
@Path("/server/")
public class ServerResource {

    private Logger log = Logger.getLogger(this.getClass().getName());

    @Context
    private SecurityContext securityContext;
    
    /**
     * Returns the currently logged in user or <code>null</code> if no user logged in.
     * 
     * @return HTTP Response
     */
    @GET
    @Path("getUser")
    public Response getUser(@Context HttpServletRequest req) {
        log.fine("getUser invoked");
        Principal principal = securityContext.getUserPrincipal();
        String userName = principal != null ? principal.getName() : null;
        return Response.ok(userName).build();
    }
    
    /**
     * Logs out the current user by invalidating the session.
     * @return an empty HTTP Response
     */
    @GET
    @Path("logout")
    public Response logout(@Context HttpServletRequest req) {
        log.fine("logout invoked");
        req.getSession().invalidate();
        return Response.ok().build();
    }
}
