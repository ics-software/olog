FROM jboss/wildfly:10.1.0.Final
LABEL maintainer="anders.harrisson@esss.se"

# PostgreSQL jdbc driver module
ENV DATABASE_DRIVER postgresql-42.2.0.jar
ADD --chown=jboss:jboss https://artifactory.esss.lu.se/artifactory/repo1/org/postgresql/postgresql/42.2.0/postgresql-42.2.0.jar /opt/jboss/wildfly/standalone/deployments/

# Standalone configuration
COPY --chown=jboss:jboss src/main/resources/wildfly/standalone.xml /opt/jboss/wildfly/standalone/configuration/

# Deployment unit
COPY target/olog-service-*.war /opt/jboss/wildfly/standalone/deployments/
